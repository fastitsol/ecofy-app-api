<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllLog extends Model
{
    protected $fillable = [
        'user_id',
        'date',
        'startingPoint',
        'destination',
        'userGivenValue',
        'logType',
        'transportType',
        'FuelEconomy',
        'utlityType',
        'dayOfMonth',
        'purchaseType',
        'divideBy',
        'overallDiet',
        'mealType',
        'month',
        'logResult'
    ];
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
