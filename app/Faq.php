<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        'topic_id','question','answer'
    ];
    public function faq_topic(){
        return $this->belongsTo('App\FaqTopic','topic_id');
    }
}
