<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    protected $fillable = [
        'user_id',
        'date',
        'purchaseType',
        'divideBy',
        'purchaseAmount',
        'log'
    ];
}
