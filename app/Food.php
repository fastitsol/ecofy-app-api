<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = [
        'user_id',
        'date',
        'mealType',
        'overallDiet',
        'consumedCalories',
        'log'
    ];
}
