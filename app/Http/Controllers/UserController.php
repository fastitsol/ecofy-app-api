<?php

namespace App\Http\Controllers;

use App\User;
use App\LifeSetting;
use App\Mail\TestEmail;
use Mail;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    protected $url = "http://pgadmin.appifylab.com";
    public function authenticate(Request $request)
    {

        // \Log::info($request);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::user();
        }

        $input = $request->only('email', 'password');
        $jwt_token = null;
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        // get the user 
        $user = Auth::user();
        // if($user->status == 'inactive') return response()->json([
        //     'success' => false,
        //     'message' => 'Please activate your account',
        // ], 401);

        // User::where('id',$user->id)->update(['app_token' => $request->app_token]);
        // $member = User::whereHas('member', function ($query) use ($user) {$query->where('user_id',$user->id)->where('status','active');})
        // ->count();

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
            // 'member' => $member,
            'user' => $user
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            // 'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            // 'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        $token = JWTAuth::fromUser($user);
        $LifeSetting = LifeSetting::create(['user_id' => $user->id]);

        return response()->json([
            'user' => $user,
            // 'LifeSetting' => $LifeSetting,
            'token' => $token
        ], 200);
        // return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

    public function showProfile(Request $request)
    {
        $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $profileUser = User::where('id', $user->id)->first();

        return response()->json([
            'profileUser' => $profileUser,
            'success' => true
        ], 200);
    }

    public function profileEdit(Request $request)
    {
        $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        if (isset($data['token'])) unset($data['token']);
        $userprofileEdit = User::where('id', $user->id)->update($data);
        if ($userprofileEdit) {
            $c_user = User::where('id', $user->id)->first();
        }
        return response()->json([
            'user' => $c_user,
            'success' => true
        ], 200);
    }

    public function upload(Request $request)
    {
        // \Log::info($request->all());
        // $url = $request->url;
        request()->file('image')->store('uploads');
        $pic = $request->image->hashName();
        $pic = "http://app.ecofy.online/uploads/$pic";
        /*update the profile pic*/
        //return Gallery::create($data);
        return response()->json([
            'image' => $pic
        ], 200);
    }

    // public function makeCode($id) {
    //     $codeId = $id * 27;
    //     $code = Math.floor(1000 + Math.random() * 9000);
    //     return $code + '' + $codeId

    // }

    public function sendPasswordResetCode(Request $request)
    {
        $email = $request->email;

        if (!$email) {
            return response()->json([
                'message' => 'Invalid Request!'
            ], 401);
        }

        $check = User::where('email', $email)->count();
        if ($check == 0) {
            return response()->json([
                'message' => 'There is no account with that email!'
            ], 401);
        }
        // $token=str_random(30);
        // \Log::info($token);
        // return;
        if ($check) {
            $token = mt_rand(1000000000, 9999999999);
            // $token=str_random(30);
        }
        // $token=str_random(30);
        // $code = this.makeCode($check->id)

        User::where('email', $email)->update(['passwordResetToken' => $token]);
        $user = User::where('email', $email)->first();

        $data = [
            'code' => $token
        ];
        // $data = [
        //     'message' => 'This is a test!'
        // ];
        Mail::send('test', $data, function ($message) use ($email) {
            $message->from('dazzling.cloudlet@gmail.com', 'Just Laravel');
            $message->to($email)->subject('Just Laravel demo email using SendGrid');
        });
        // Mail::to($email)->send(new TestEmail($data));

        return response()->json([
            'user' => $user
        ], 200);
    }

    public function verifyPasswordResetCode(Request $request)
    {
        // $data = $request->all();
        $datatoken = $request->token;

        if (!$datatoken) {
            return response()->json([
                'message' => 'Invalid Request!'
            ], 401);
        }

        $check = User::where('passwordResetToken', $datatoken)->count();
        if ($check == 1) {
            return 'Token verified!';
        };
        return response()->json([
            'message' => 'Invalid Code'
        ], 401);
    }

    public function resetPassword(Request $request)
    {
        // $data = $request->all();
        $validator = Validator::make($request->all(), [
            'newPassword' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $password = Hash::make($request->get('newPassword'));
        return User::where('passwordResetToken', $request->token)->update(['password' => $password, 'passwordResetToken' => NULL]);

        // if (!$datatoken) {
        //     return response()->json([
        //         'message'=> 'Invalid Request!'
        //     ],401);
        // }

        // $check = User::where('passwordResetToken',$datatoken)->count();
        // if ($check == 1) {
        //     return 'Token verified!';
        // };
        // return response()->json([
        //     'message'=> 'Invalid Code'
        // ],401);
    }
}
