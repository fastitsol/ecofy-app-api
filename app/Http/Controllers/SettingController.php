<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LifeSetting;
use App\User;
use JWTAuth;
use Auth;

class SettingController extends Controller
{
    public function lifeSettingUpdate(Request $request){
        $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
            'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $data=[
            'mainTransportation'=> $request->mainTransportation,
            'avgCarMpg'=> $request->avgCarMpg,
            'avgDriverDistance'=> $request->avgDriverDistance,
            'avgAnuallyFlownHr'=> $request->avgAnuallyFlownHr,
            'householdSize'=> $request->householdSize,
            'renewableEnergy'=> $request->renewableEnergy,
            'electricityUtility'=> $request->electricityUtility,
            'avgNonVegeterian'=> $request->avgNonVegeterian,
        ];
        $LifeSetting = LifeSetting::where('user_id',$user_id)->update($data); 
        return response()->json([
            'lifeSetting' => $LifeSetting,
            'success' => true
        ],200);
         
    }

    public function showLifeSetting(Request $request){
        // $data = $request->all();
        if(!User::checkToken($request)){
            return response()->json([
                'message' => 'Token is required'
            ],422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $LifeSetting = LifeSetting::where('user_id',$user_id)->get();
        return response()->json([
            'lifeSetting' => $LifeSetting,
            'success' => true
        ],200);
    }
}
