<?php

namespace App\Http\Controllers;

use App\PaypalInfo;
use App\User;
use App\Food;
use App\Shopping;
use App\ContactUs;
use App\AllLog;
use App\About;
use App\NewsPage;
use App\Faq;
use App\FaqTopic;
use App\Recommandation;
use App\Question;
use App\QuestionChoice;
use App\UserQuestionAnswer;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Auth;
use Illuminate\Http\Request;

class AppsController extends Controller
{
    protected $url = "http://pgadmin.appifylab.com";

    public function showSingleLog(Request $request, $id)
    {
        $singleLog = AllLog::where('id', $id)->first();
        return response()->json([
            'singleLog' => $singleLog,
            'success' => true
        ], 200);
    }


    public function addToContact(Request $request)
    {
        $data = $request->all();
        $ContactUs = ContactUs::create($data);
        return response()->json([
            'contactUs' => $ContactUs,
            'success' => true
        ], 200);
    }

    ////////////////////////////// add minus type log on successful donation start /////////////////////////////
    public function addMinusLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'logResult' => -($request->logResult),
        ];
        // \Log::info($data);
        $minus = AllLog::create($data);
        return response()->json([
            'minus' => $minus,
            'success' => true
        ], 200);
    }
    ////////////////////////////// add minus type log on successful donation end /////////////////////////////



    public function addTransportLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $transportType = $request->transportType;
        $milesDriven = $request->milesDriven;
        $FuelEconomy = $request->FuelEconomy;
        $Manufacturing = (0.051 * $milesDriven);
        $FuelCombustion = (8.8 / $FuelEconomy) * $milesDriven;
        $FuelProduction  = (1.64 / $FuelEconomy) * $milesDriven;
        if ($transportType == 'car' || $transportType == 'bike') {
            $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction);
        } elseif ($transportType == 'bus') {
            $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.667;
        } elseif ($transportType == 'train') {
            $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.316;
        } elseif ($transportType == 'carpool') {
            $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.48;
        } elseif ($transportType == 'vanpool') {
            $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.229;
        }

        $log = ($carbonEmissions * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'startingPoint' => $request->startingPoint,
            'destination' => $request->destination,
            'date' => $request->date,
            'logType' => $request->logType,
            'transportType' => $request->transportType,
            'FuelEconomy' => $request->FuelEconomy,
            'userGivenValue' => $milesDriven,
            'logResult' => $log
        ];
        // \Log::info($data);
        $transport = AllLog::create($data);
        return response()->json([
            'transport' => $transport,
            'success' => true
        ], 200);
    }

    public function editLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $logType = $request->logType;
        if ($logType == 'transport') {
            $transportType = $request->transportType;
            $milesDriven = $request->userGivenValue;
            $FuelEconomy = $request->FuelEconomy;
            $Manufacturing = (0.051 * $milesDriven);
            $FuelCombustion = (8.8 / $FuelEconomy) * $milesDriven;
            $FuelProduction  = (1.64 / $FuelEconomy) * $milesDriven;
            if ($transportType == 'car' || $transportType == 'bike') {
                $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction);
            } elseif ($transportType == 'bus') {
                $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.667;
            } elseif ($transportType == 'train') {
                $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.316;
            } elseif ($transportType == 'carpool') {
                $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.48;
            } elseif ($transportType == 'vanpool') {
                $carbonEmissions = ($Manufacturing + $FuelCombustion + $FuelProduction) * 0.229;
            }

            $log = ($carbonEmissions * 2.2);
            // \Log::info($log);
            $data = [
                'user_id' => $user_id,
                'date' => $request->date,
                'startingPoint' => $request->startingPoint,
                'destination' => $request->destination,
                'logType' => $request->logType,
                'transportType' => $request->transportType,
                'FuelEconomy' => $request->FuelEconomy,
                'userGivenValue' => $milesDriven,
                'logResult' => $log
            ];
            $updatedLog = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
            // if($transport){
            //     $updatedLog = AllLog::where([['id',$request->id],['user_id',$user_id]])->first();
            // }
        } elseif ($logType == 'utility') {
            $currentYear = date('Y');
            $utlityType = $request->utlityType;
            $utlityBill = $request->userGivenValue;
            $month = $request->month;
            $dayOfMonth = cal_days_in_month(CAL_GREGORIAN, $month, $currentYear);
            // $dayOfMonth = $request->dayOfMonth;
            $log = ($utlityType * $utlityBill) / $dayOfMonth;
            $logResult = ($log * 2.2);
            // \Log::info($log);
            $data = [
                'user_id' => $user_id,
                'date' => $request->date,
                'logType' => $request->logType,
                'utlityType' => $request->utlityType,
                'month' => $request->month,
                'dayOfMonth' => $dayOfMonth,
                'userGivenValue' => $utlityBill,
                'logResult' => $logResult

            ];
            $updatedLog = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
            // if($utilities){
            //     $updatedLog = AllLog::where([['id',$request->id],['user_id',$user_id]])->first();
            // }
        } elseif ($logType == 'shopping') {
            $purchaseType = $request->purchaseType;
            $purchaseAmount = $request->userGivenValue;
            $divideBy = $request->divideBy;
            $log = ($purchaseType * $purchaseAmount) / $divideBy;
            $logResult = ($log * 2.2);
            // \Log::info($log);
            $data = [
                'user_id' => $user_id,
                'date' => $request->date,
                'logType' => $request->logType,
                'purchaseType' => $request->purchaseType,
                'divideBy' => $request->divideBy,
                'userGivenValue' => $purchaseAmount,
                'logResult' => $logResult
            ];
            $updatedLog = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
            // if($Shopping){
            //     $updatedLog = AllLog::where([['id',$request->id],['user_id',$user_id]])->first();
            // }
        } elseif ($logType == 'food') {
            $overallDiet = $request->overallDiet;
            $consumedCalories = $request->userGivenValue;
            $log = ($overallDiet * $consumedCalories) / 2600;
            $logResult = ($log * 2.2);
            $data = [
                'user_id' => $user_id,
                'date' => $request->date,
                'logType' => $request->logType,
                'mealType' => $request->mealType,
                'overallDiet' => $request->overallDiet,
                'userGivenValue' => $consumedCalories,
                'logResult' => $logResult

            ];

            $updatedLog = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
            // if($food){
            //     $updatedLog = AllLog::where([['id',$request->id],['user_id',$user_id]])->first();
            // }
        }
        return response()->json([
            'updatedLog' => $updatedLog,
            'success' => true
        ], 200);
    }

    public function addUtilitiesLog(Request $request)
    {
        // $currentMonth = '11';
        // $currentMonth = date('m');
        $currentYear = date('Y');
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $utlityType = $request->utlityType;
        $utlityBill = $request->utlityBill;
        $month = $request->month;
        $dayOfMonth = cal_days_in_month(CAL_GREGORIAN, $month, $currentYear);
        // $dayOfMonth = $request->dayOfMonth;
        $log = ($utlityType * $utlityBill) / $dayOfMonth;
        $logResult = ($log * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'utlityType' => $request->utlityType,
            'month' => $request->month,
            'dayOfMonth' => $dayOfMonth,
            'userGivenValue' => $utlityBill,
            'logResult' => $logResult
        ];
        // \Log::info($data);
        $utilities = AllLog::create($data);
        return response()->json([
            // 'currentMonth' => $currentMonth,
            'currentYear' => $currentYear,
            // 'days' => $days,
            'utilities' => $utilities,
            'success' => true
        ], 200);
    }

    public function editUtilitiesLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $utlityType = $request->utlityType;
        $utlityBill = $request->utlityBill;
        $dayOfMonth = $request->dayOfMonth;
        $log = ($utlityType * $utlityBill) / $dayOfMonth;
        $logResult = ($log * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'userGivenValue' => $utlityBill,
            'logResult' => $logResult
        ];
        $utilities = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
        if ($utilities) {
            $updatedutilities = AllLog::where([['id', $request->id], ['user_id', $user_id]])->first();
        }
        return response()->json([
            'updatedutilities' => $updatedutilities,
            'success' => true
        ], 200);
    }

    public function addShoppingLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $purchaseType = $request->purchaseType;
        $purchaseAmount = $request->purchaseAmount;
        $divideBy = $request->divideBy;
        $log = ($purchaseType * $purchaseAmount) / $divideBy;
        $logResult = ($log * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'purchaseType' => $request->purchaseType,
            'divideBy' => $request->divideBy,
            'userGivenValue' => $purchaseAmount,
            'logResult' => $logResult
        ];
        // \Log::info($data);
        $Shopping = AllLog::create($data);
        return response()->json([
            'shopping' => $Shopping,
            'success' => true
        ], 200);
    }

    public function editShoppingLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $purchaseType = $request->purchaseType;
        $purchaseAmount = $request->purchaseAmount;
        $divideBy = $request->divideBy;
        $log = ($purchaseType * $purchaseAmount) / $divideBy;
        $logResult = ($log * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'userGivenValue' => $purchaseAmount,
            'logResult' => $logResult
        ];
        $Shopping = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
        if ($Shopping) {
            $updatedShopping = AllLog::where([['id', $request->id], ['user_id', $user_id]])->first();
        }
        return response()->json([
            'updatedShopping' => $updatedShopping,
            'success' => true
        ], 200);
    }

    public function addFoodLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $overallDiet = $request->overallDiet;
        $consumedCalories = $request->consumedCalories;
        $log = ($overallDiet * $consumedCalories) / 2600;
        $logResult = ($log * 2.2);
        // \Log::info($log);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'mealType' => $request->mealType,
            'logType' => $request->logType,
            'overallDiet' => $request->overallDiet,
            'userGivenValue' => $consumedCalories,
            'logResult' => $logResult
        ];
        // \Log::info($data);
        $food = AllLog::create($data);
        return response()->json([
            'food' => $food,
            'success' => true
        ], 200);
    }

    public function editFoodLog(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $overallDiet = $request->overallDiet;
        $consumedCalories = $request->consumedCalories;
        $log = ($overallDiet * $consumedCalories) / 2600;
        $logResult = ($log * 2.2);
        $data = [
            'user_id' => $user_id,
            'date' => $request->date,
            'logType' => $request->logType,
            'userGivenValue' => $consumedCalories,
            'logResult' => $logResult
        ];

        $food = AllLog::where([['id', $request->id], ['user_id', $user_id]])->update($data);
        if ($food) {
            $updatedfood = AllLog::where([['id', $request->id], ['user_id', $user_id]])->first();
        }
        return response()->json([
            'updatedfood' => $updatedfood,
            'success' => true
        ], 200);
    }

    public function showLogs(Request $request)
    {
        $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $task = AllLog::where('user_id', $user->id)
            ->select(DB::raw(' * , DATE_FORMAT(date, "%Y-%m-%d") AS logdate'))
            ->where('logType', '!=', 'minus')
            ->orderBy('date', 'desc')
            ->get();
        // $data = $task->whereNotNull('date')->orderBy('id','desc')->get();
        $value = $task->groupBy('logdate');
        $Food = [];
        foreach ($value as $key => $v) {
            $t = [];
            $t['date'] = $key;
            $t['data'] = $v;
            array_push($Food, $t);
        }
        return response()->json([
            'log' => $Food,
            // 'value' => $value,
            'success' => true
        ], 200);
    }
    public function showLogbySearch(Request $request)
    {
        $date = $request->date;
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $AllLog = AllLog::where('user_id', $user->id)->whereDate('date', $date)->where('logType', '!=', 'minus')->orderBy('date', 'desc')->get();
        return response()->json([
            'allLogbySearch' => $AllLog,
            'success' => true
        ], 200);
    }

    public function showGraphForTodayCarbonEmission(Request $request)
    {
        $foods = [];
        $currentMonth = date('m');
        $today = date("Y-m-d");
        $startedMonth = date("Y-m-d");
        $month = date("F j");
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $food = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'food']])->whereDate('date', $today)->first();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'shopping']])->whereDate('date', $today)->first();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'utility']])->whereDate('date', $today)->first();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'transport']])->whereDate('date', $today)->first();
        return response()->json([
            'food' => $food,
            'shopping' => $shopping,
            'utility' => $utility,
            'transport' => $transport,
            'month' => $month,
            'success' => true
        ], 200);
    }
    public function showGraphForMonthlyCarbonEmission(Request $request)
    {
        $foods = [];
        $currentMonth = date('m');
        $currentYear = date('Y');
        $today = date("Y-m-d");
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        // $days = cal_days_in_month(CAL_GREGORIAN,$currentMonth,$currentYear);
        $foods = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'date')->where([['user_id', $user->id], ['logType', 'food']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->groupBy('date')->orderBy('date', 'asc')->get();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'date')->where([['user_id', $user->id], ['logType', 'shopping']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->groupBy('date')->orderBy('date', 'asc')->get();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'date')->where([['user_id', $user->id], ['logType', 'utility']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->groupBy('date')->orderBy('date', 'asc')->get();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'date')->where([['user_id', $user->id], ['logType', 'transport']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->groupBy('date')->orderBy('date', 'asc')->get();
        return response()->json([
            'food' => $foods,
            'shopping' => $shopping,
            'utility' => $utility,
            'transport' => $transport,
            'currentMonth' => $currentMonth,
            // 'days' => $days,
            // 'value' => $value,
            'success' => true
        ], 200);
    }
    public function showGraphForYearlyCarbonEmission(Request $request)
    {
        $foods = [];
        $currentYear = date('Y');
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        // $food = AllLog::select(DB::raw( 'SUM(logResult) AS logResult'))->where([['user_id',$user->id],['logType','food']])->whereYear('date',$currentYear)->first();
        // $foods = AllLog::select(DB::raw( 'SUM(logResult) AS logResult'),'date')->where([['user_id',$user->id],['logType','food']])->whereYear('date',$currentYear)->groupBy('date')->orderBy('date','asc')->get();
        $foods = AllLog::select(DB::raw('SUM(logResult) AS logResult'), DB::raw("MONTHNAME(date) as monthname"))->where([['user_id', $user->id], ['logType', 'food']])->whereYear('date', $currentYear)->groupBy('monthname')->orderBy('date', 'asc')->get();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'), DB::raw("MONTHNAME(date) as monthname"))->where([['user_id', $user->id], ['logType', 'shopping']])->whereYear('date', $currentYear)->groupBy('monthname')->orderBy('date', 'asc')->get();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'), DB::raw("MONTHNAME(date) as monthname"))->where([['user_id', $user->id], ['logType', 'utility']])->whereYear('date', $currentYear)->groupBy('monthname')->orderBy('date', 'asc')->get();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'), DB::raw("MONTHNAME(date) as monthname"))->where([['user_id', $user->id], ['logType', 'transport']])->whereYear('date', $currentYear)->groupBy('monthname')->orderBy('date', 'asc')->get();
        return response()->json([
            'food' => $foods,
            'shopping' => $shopping,
            'utility' => $utility,
            'transport' => $transport,
            'currentYear' => $currentYear,
            'success' => true
        ], 200);
    }
    public function showTreeToDonateForToday(Request $request)
    {
        $foods = [];
        $currentMonth = date('m');
        $today = date("Y-m-d");
        $startedMonth = date("Y-m-d");
        $month = date("F j");
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $food = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'food']])->whereDate('date', $today)->first();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'shopping']])->whereDate('date', $today)->first();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'utility']])->whereDate('date', $today)->first();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'transport']])->whereDate('date', $today)->first();
        $minus = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'minus']])->whereDate('date', $today)->first();
        $totalFood = (($food->logResult) / 2.2);
        $totalShopping = (($shopping->logResult) / 2.2);
        $totalUtility = (($utility->logResult) / 2.2);
        $totalTransport = (($transport->logResult) / 2.2);
        $total = $totalFood + $totalShopping + $totalUtility + $totalTransport;
        if (($total + ($minus->logResult)) < 0) $total = 0;
        else $total = ($total + ($minus->logResult));
        // if ($total == 0) {
        //     return response()->json([
        //         'message' => "You haven't emmitted carbon yet"
        //     ], 200);
        // }
        if ($total == 0 && $request->cost) {
            $treesNeeded = ($request->cost / 0.50); // 2/0.5=4 
            $percentage = 0;
            // $percentageofTressNeeded = 0;
            $percentageofTressNeeded = ($request->cost / 0.50);
            $cost = $request->cost;
            $percentageofCost = $request->cost;
        } elseif ($request->cost) {
            $treesNeeded = floor((2.52 * $total) / 54.8); // !4
            $cost = (0.50 * $treesNeeded);
            $percentageofCost = $request->cost;
            $percentage = ($percentageofCost * 100) / $cost;
            $percentageofTressNeeded = ($treesNeeded * $percentage) / 100;
        } else {
            $treesNeeded = floor((2.52 * $total) / 54.8);
            $percentage = $request->percentage;
            $percentageofTressNeeded = ($treesNeeded * $percentage) / 100;
            $cost = (0.50 * $treesNeeded);
            $percentageofCost = ($cost * $percentage) / 100;
        }

        return response()->json([
            'total' => $total,
            'totalCost' => $cost,
            'treesNeeded' => $treesNeeded,
            'percentage' => $percentage,
            'percentageofTressNeeded' => $percentageofTressNeeded,
            'percentageofCost' => $percentageofCost,
            'minus' => $minus,
            'success' => true
        ], 200);
    }
    public function showTodayCarbonEmission(Request $request)
    {
        $foods = [];
        $currentMonth = date('m');
        $today = date("Y-m-d");
        $startedMonth = date("Y-m-d");
        $month = date("F j");
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $food = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'food']])->whereDate('date', $today)->first();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'shopping']])->whereDate('date', $today)->first();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'utility']])->whereDate('date', $today)->first();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'transport']])->whereDate('date', $today)->first();
        $minus = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'minus']])->whereDate('date', $today)->first();
        $total = $food->logResult + $shopping->logResult + $utility->logResult + $transport->logResult;
        $totalMinus = ($total + ($minus->logResult));
        if ($total == 0) {
            return response()->json([
                'message' => "You haven't emmitted carbon yet"
            ], 200);
        }
        $foodPercent = (($food->logResult * 100) / $total);
        $shoppingPercent = (($shopping->logResult * 100) / $total);
        $utilityPercent = (($utility->logResult * 100) / $total);
        $transportPercent = (($transport->logResult * 100) / $total);
        $totalCarbonEmission = ($total);
        return response()->json([
            'foodPercent' => $foodPercent,
            'shoppingPercent' => $shoppingPercent,
            'utilityPercent' => $utilityPercent,
            'transportPercent' => $transportPercent,
            // 'totalCarbonEmission' => $totalCarbonEmission,
            'totalCarbonEmission' => $totalMinus,
            // 'currentMonth' => $currentMonth,
            'date' => $today,
            'month' => $month,
            'success' => true
        ], 200);
    }

    public function showMonthlyCarbonEmission(Request $request)
    {
        $foods = [];
        $currentMonth = date('m');
        $currentYear = date('Y');
        // $data = DB::table("items")
        //     ->whereRaw('MONTH(created_at) = ?',[$currentMonth])
        //     ->get();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $food = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'food']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->first();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'shopping']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->first();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'utility']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->first();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'transport']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->first();
        $minus = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'minus']])->whereMonth('date', $currentMonth)->whereYear('date', $currentYear)->first();
        $total = $food->logResult + $shopping->logResult + $utility->logResult + $transport->logResult;
        $totalMinus = ($total + ($minus->logResult));
        if ($total == 0) {
            return response()->json([
                'message' => "You haven't emmitted carbon yet"
            ], 200);
        }
        $foodPercent = (($food->logResult * 100) / $total);
        $shoppingPercent = (($shopping->logResult * 100) / $total);
        $utilityPercent = (($utility->logResult * 100) / $total);
        $transportPercent = (($transport->logResult * 100) / $total);
        $totalCarbonEmission = ($total);
        return response()->json([
            'foodPercent' => $foodPercent,
            'shoppingPercent' => $shoppingPercent,
            'utilityPercent' => $utilityPercent,
            'transportPercent' => $transportPercent,
            // 'totalCarbonEmission' => $totalCarbonEmission,
            'totalCarbonEmission' => $totalMinus,
            // 'date' => $today,
            'currentMonth' => $currentMonth,
            'success' => true
        ], 200);
    }

    public function showYearlyCarbonEmission(Request $request)
    {
        $foods = [];
        $currentYear = date('Y');
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();

        $food = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'food']])->whereYear('date', $currentYear)->first();
        $shopping = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'shopping']])->whereYear('date', $currentYear)->first();
        $utility = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'utility']])->whereYear('date', $currentYear)->first();
        $transport = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'transport']])->whereYear('date', $currentYear)->first();
        $minus = AllLog::select(DB::raw('SUM(logResult) AS logResult'))->where([['user_id', $user->id], ['logType', 'minus']])->whereYear('date', $currentYear)->first();
        $total = $food->logResult + $shopping->logResult + $utility->logResult + $transport->logResult;
        $totalMinus = ($total + ($minus->logResult));
        if ($total == 0) {
            return response()->json([
                'message' => "You haven't emmitted carbon yet"
            ], 200);
        }
        $foodPercent = (($food->logResult * 100) / $total);
        $shoppingPercent = (($shopping->logResult * 100) / $total);
        $utilityPercent = (($utility->logResult * 100) / $total);
        $transportPercent = (($transport->logResult * 100) / $total);
        $totalCarbonEmission = ($total);
        return response()->json([
            'foodPercent' => $foodPercent,
            'shoppingPercent' => $shoppingPercent,
            'utilityPercent' => $utilityPercent,
            'transportPercent' => $transportPercent,
            'totalCarbonEmission' => $totalMinus,
            // 'totalCarbonEmission' => $totalCarbonEmission,
            // 'date' => $today,
            'currentYear' => $currentYear,
            'success' => true
        ], 200);
    }


    public function showLeaderBoard(Request $request)
    {
        $today = date("Y-m-d");
        $array = [];

        $vari = 8818;
        // $data = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'user_id')->get();
        $learder = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'user_id')
            ->whereDate('date', $today)
            ->with('user')
            ->groupBy('user_id')
            ->orderBy('logResult', 'asc')
            // ->where('logResult', '>', $vari)
            ->get();


        return response()->json([
            'learder' => $learder,
            'success' => true
        ], 200);
    }

    public function showLeaderBoardUserRank(Request $request)
    {
        $rank = 0;
        $array = [];
        $today = date("Y-m-d");
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();

        $learder = AllLog::select(DB::raw('SUM(logResult) AS logResult'), 'user_id')
            ->whereDate('date', $today)
            // ->where('logResult','>',8818)
            ->with('user')
            ->groupBy('user_id')
            ->orderBy('logResult', 'asc')
            ->get();
        // return $learder;

        // foreach($learder as $key=>$values)
        // {

        //     if($values['logResult'] < 8818){
        //         // $leader = $values['logResult'];
        //         $pp = $key;
        //         // unset($learder['logResult']);
        //      unset($learder[$pp]);
        //     }
        // }
        // array_replace($learder,$learder);
        // array_push($array,$learder);
        // array_slice($learder,$key);



        // foreach($learder as $key=>$values)
        // {

        //     if($values['logResult'] < 8818){
        //         // $leader = $values['logResult'];
        //         $pp = $key;
        //         // unset($learder['logResult']);
        //          unset($learder[$key]);
        //     }
        //     if($values['user_id'] == $user->id){
        //         $rank = $key+1;
        //     }
        // }

        foreach ($learder as $key => $values) {
            if ($values['user_id'] == $user->id) {
                $rank = $key + 1;
            }
        }

        $totalLeader = $learder->count();

        return response()->json([
            'totalLeader' => $totalLeader,
            'userRank' => $rank,
            'learder' => $learder,
            'success' => true
        ], 200);
    }
    public function getAbout()
    {
        $about = About::orderBy('id', 'desc')->get();
        return response()->json([
            'about' => $about,
            'success' => true
        ], 200);
    }

    public function addContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $Contact = ContactUs::create([
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message'),
            'attachment' => $request->attachment,
        ]);

        return response()->json([
            'contact' => $Contact,
            'success' => true
        ], 200);
    }
    public function upload(Request $request)
    {
        // \Log::info($request->all());
        // $url = $request->url;
        request()->file('attachment')->store('uploads');
        $attachment = $request->attachment->hashName();
        $attachment = "http://app.ecofy.online/uploads/$attachment";
        // $attachment= $this->$url."/uploads/$attachment";
        /*update the profile pic*/
        //return Gallery::create($data);
        return response()->json([
            'attachment' => $attachment
        ], 200);
    }
    public function getNews()
    {
        $NewsPage = NewsPage::orderBy('id', 'desc')->get();
        return response()->json([
            'allnews' => $NewsPage
        ], 200);
    }
    public function getSingleNews(Request $request, $id)
    {
        $NewsPage = NewsPage::where('id', $id)->first();
        return response()->json([
            'singleNews' => $NewsPage,
            'success' => true
        ], 200);
    }
    public function getFaq()
    {
        $faq = Faq::with('faq_topic')->orderBy('id', 'asc')->get();
        return response()->json([
            'faq' => $faq,
            'success' => true
        ], 200);
    }
    public function getFaqTopic()
    {
        $FaqTopic = FaqTopic::with('faq')->orderBy('id', 'asc')->get();
        return response()->json([
            'faqTopic' => $FaqTopic,
            'success' => true
        ], 200);
    }
    public function getFoodRecommendation()
    {
        $foodRecommendation = Recommandation::where('type', 'food')->orderBy('id', 'desc')->get();
        return response()->json([
            'foodRecommendation' => $foodRecommendation,
            'success' => true
        ], 200);
    }
    public function getTransportRecommendation()
    {
        $transportRecommendation = Recommandation::where('type', 'transport')->orderBy('id', 'desc')->get();
        return response()->json([
            'transportRecommendation' => $transportRecommendation,
            'success' => true
        ], 200);
    }
    public function getUtilityRecommendation()
    {
        $utilityRecommendation = Recommandation::where('type', 'utility')->orderBy('id', 'desc')->get();
        return response()->json([
            'utilityRecommendation' => $utilityRecommendation,
            'success' => true
        ], 200);
    }
    public function getSingleRecommandation(Request $request, $id)
    {
        $singleRecommendation = Recommandation::where('id', $id)->orderBy('id', 'desc')->first();
        return response()->json([
            'singleRecommendation' => $singleRecommendation,
            'success' => true
        ], 200);
    }
    public function getQuestionChoice()
    {
        $QuestionChoice = Question::with('choices')->orderBy('id', 'asc')->get();
        return response()->json([
            'QuestionChoice' => $QuestionChoice,
            'success' => true
        ], 200);
    }
    public function addUserQuizAnswer(Request $request)
    {
        // $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $question_id = $request->question_id;
        $choice_id = $request->choice_id;
        $rightChoice = QuestionChoice::where([['question_id', $question_id], ['id', $choice_id], ['is_right_choice', 1]])->first();
        if ($rightChoice) {
            $data = [
                'user_id' => $user_id,
                'question_id' => $request->question_id,
                'choice_id' => $request->choice_id,
                'is_right' => '1',
            ];
            $UserQuestionAnswer = UserQuestionAnswer::create($data);
        } else {
            $data = [
                'user_id' => $user_id,
                'question_id' => $request->question_id,
                'choice_id' => $request->choice_id,
                'is_right' => '0',
            ];
            $UserQuestionAnswer = UserQuestionAnswer::create($data);
        }
        return response()->json([
            'userQuestionAnswer' => $UserQuestionAnswer,
            'success' => true
        ], 200);
    }
    public function showUserQuizScore(Request $request)
    {
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();

        $userScore = UserQuestionAnswer::select(DB::raw('SUM(is_right) AS is_right'), 'user_id')
            ->where('user_id', $user->id)
            ->groupBy('user_id')
            ->first();
        // foreach($userScore as $key=>$values)
        // {
        //     if($values['user_id'] == $user->id){
        //         $score = $key+1;
        //     }
        // }
        $totalQuestion = Question::count();

        return response()->json([
            'userScore' => $userScore,
            // 'score' => $score,
            'totalQuestion' => $totalQuestion,
            'success' => true
        ], 200);
    }
    public function deleteUserQuestionAnswer(Request $request)
    {
        $data = $request->all();
        if (!User::checkToken($request)) {
            return response()->json([
                'message' => 'Token is required'
            ], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        return UserQuestionAnswer::where('user_id', $user->id)->delete();
        // return response()->json([
        //     'userScore' => $userScore,
        //     // 'score' => $score,
        //     'totalQuestion' => $totalQuestion,
        //     'success' => true
        // ],200);
    }

    // for api credentials
    public function get_PayPalInfo()
    {
        $PaypalInfo = PaypalInfo::orderBy('id', 'desc')->get();
        return response()->json([
            'paypalInfo' => $PaypalInfo
        ], 200);
    }
}
