<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionChoice extends Model
{
    protected $fillable = [
        'question_id','choice','is_right_choice'
    ];
    public function question(){
        return $this->belongsTo('App\Question','question_id');
    }
}
