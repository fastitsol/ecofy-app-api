<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalInfo extends Model
{
    protected $fillable = [
        'client_id', 'secret_key'
    ];
}
