<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LifeSetting extends Model
{
    protected $fillable = [
        'user_id',
        'mainTransportation',
        'avgCarMpg',
        'avgDriverDistance',
        'avgAnuallyFlownHr',
        'householdSize', 
        'renewableEnergy', 
        'electricityUtility',
        'avgNonVegeterian'
    ];
}
