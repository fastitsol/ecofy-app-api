<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqTopic extends Model
{
    protected $fillable = [
        'name'
    ];
    public function faq()
    {
        return $this->hasMany('App\Faq','topic_id');
    }
}
