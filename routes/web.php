<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::post('/app/register', 'UserController@register');
Route::post('/app/login', 'UserController@authenticate');
Route::get('/app/showProfile', 'UserController@showProfile');
Route::post('/app/profileEdit', 'UserController@profileEdit');
Route::post('/app/profileImageUpload', 'UserController@upload');
Route::post('/app/sendPasswordResetCode', 'UserController@sendPasswordResetCode');
Route::post('/app/verifyPasswordResetCode', 'UserController@verifyPasswordResetCode');
Route::post('/app/resetPassword', 'UserController@resetPassword');

// for lifesetting page
Route::post('/app/lifeSettingUpdate', 'SettingController@lifeSettingUpdate');
Route::get('/app/showLifeSetting', 'SettingController@showLifeSetting');

// for Contact us page
Route::post('/app/addToContact', 'AppsController@addToContact');

// for calculate page
Route::post('/app/addFoodLog', 'AppsController@addFoodLog');
Route::post('/app/addShoppingLog', 'AppsController@addShoppingLog');
Route::post('/app/addUtilitiesLog', 'AppsController@addUtilitiesLog');
Route::post('/app/addTransportLog', 'AppsController@addTransportLog');
//
Route::post('/app/addMinusLog', 'AppsController@addMinusLog');

// for log page
Route::post('/app/editLog', 'AppsController@editLog');
Route::get('/app/showLogs', 'AppsController@showLogs');
Route::get('/app/showLogbySearch', 'AppsController@showLogbySearch');
Route::get('/app/showSingleLog/{id}', 'AppsController@showSingleLog');

// for donation
Route::get('/app/showTreeToDonateForToday', 'AppsController@showTreeToDonateForToday');

// for dashboard page
Route::get('/app/showTodayCarbonEmission', 'AppsController@showTodayCarbonEmission');
Route::get('/app/showMonthlyCarbonEmission', 'AppsController@showMonthlyCarbonEmission');
Route::get('/app/showYearlyCarbonEmission', 'AppsController@showYearlyCarbonEmission');
Route::get('/app/showGraphForTodayCarbonEmission', 'AppsController@showGraphForTodayCarbonEmission');
Route::get('/app/showGraphForMonthlyCarbonEmission', 'AppsController@showGraphForMonthlyCarbonEmission');
Route::get('/app/showGraphForYearlyCarbonEmission', 'AppsController@showGraphForYearlyCarbonEmission');

// for leaderboard page
Route::get('/app/showLeaderBoard', 'AppsController@showLeaderBoard');
Route::get('/app/showLeaderBoardUserRank', 'AppsController@showLeaderBoardUserRank');

// for about
Route::get('/app/getAbout', 'AppsController@getAbout');

// for Contact
Route::post('/app/addContact', 'AppsController@addContact');
Route::post('/app/attachmentUpload', 'AppsController@upload');

// for news
Route::get('/app/getNews', 'AppsController@getNews');
Route::get('/app/getSingleNews/{id}', 'AppsController@getSingleNews');

// for FAQ
Route::get('/app/getFaq', 'AppsController@getFaq');
Route::get('/app/getFaqTopic', 'AppsController@getFaqTopic');

// for recommandation
Route::get('/app/getFoodRecommendation', 'AppsController@getFoodRecommendation');
Route::get('/app/getTransportRecommendation', 'AppsController@getTransportRecommendation');
Route::get('/app/getUtilityRecommendation', 'AppsController@getUtilityRecommendation');
Route::get('/app/getSingleRecommandation/{id}', 'AppsController@getSingleRecommandation');

// for Quiz
Route::get('/app/getQuestionChoice', 'AppsController@getQuestionChoice');
Route::post('/app/addUserQuizAnswer', 'AppsController@addUserQuizAnswer');
Route::get('/app/showUserQuizScore', 'AppsController@showUserQuizScore');
Route::post('/app/deleteUserQuestionAnswer', 'AppsController@deleteUserQuestionAnswer');

// Route::get('/app/showFoodLogs', 'AppsController@showFoodLogs');
// Route::get('/app/showFoodLogbySearch', 'AppsController@showFoodLogbySearch');

Route::get('/app/open', 'DataController@open');
Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/app/user', 'UserController@getAuthenticatedUser');
    Route::get('/app/closed', 'DataController@closed');
});


// for Paypal
Route::get('/app/get_PayPalInfo', 'AppsController@get_PayPalInfo');
